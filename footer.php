<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="wrap">
    <div class="site-info">
      <?php printf( __('<strong>%1$s</strong>. Copyright &copy; %2$s. All rights reserved.', 'fg-language-tunnel'), get_bloginfo('name'), date("Y")); ?>
    </div><!-- .site-info -->
  </div><!-- .wrap -->
</footer><!-- #colophon -->
</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
