<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function fg_lang_selector() {
  $sites = get_sites();
  $options = array();
  foreach ($sites as $site) {
    // Skip the root site
    if ($site->path != '/') {
      $hide = get_option('fg_lang_site_hidden_' . $site->blog_id, 0);
      if ($hide) {
        continue;
      }

      $path_parts = explode('-', trim($site->path, '/'));
      $weight = get_option('fg_lang_site_weight_' . $site->blog_id, 0);
      $options[$weight . '.' . $site->blog_id] = array(
        'name' => get_option('fg_lang_site_name_' . $site->blog_id, ''),
        'path' => $site->path,
        'country' => $path_parts[1]
      );
    }
  }
  ksort($options);
  return $options;
}