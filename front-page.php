<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$options = fg_lang_selector();
get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
<!--                <pre>-->
<!--                    --><?php //var_dump(get_sites()); ?>
<!--                </pre>-->
                <h1>Please choose your region:</h1>
                <?php foreach ($options as $language): ?>
                    <div class="lang-option" style="display: inline-block">
                        <a href="<?php echo $language['path'] ?>">
                            <span class="flag flag-<?php echo $language['country'] ?>"></span><br/>
                            <?php echo $language['name'] ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>
<?php get_footer();
